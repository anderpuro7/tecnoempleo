# author:"Ander Elexpuru & Ignacio Saiz"

rm(list = ls());cat("\014");graphics.off()
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
getwd()
dir()

library(tidyverse)
library(ggplot2)
library(pracma)
library(ngram)

library(pdftools)
library(dplyr)
library(stopwords)
library(stringi)
library(stringr)
library(scales)
library(tidyr)
library(widyr)
library(ggraph)
library(igraph)
library(quanteda)
library(topicmodels)
library(cvTools)
########cargamos las funciones que vamos a necesitar mas adelante######
######################
numeroTopicsOptimo<-function(dtm){
  K <- c(5,10,20, 30, 40, 50, 60, 70, 80)
  results <- list()
  
  i = 1
  for (k in K){
    cat("\n\n\n##########\n ", k, "topics", "\n")
    res <- cvLDA(k, dtm)
    results[[i]] <- res
    i = i + 1
  }
  
  ## plot
  df <- data.frame(
    k = rep(K, each=10),
    perp =  unlist(lapply(results, '[[', 'perplexity')),
    loglk = unlist(lapply(results, '[[', 'logLik')),
    stringsAsFactors=F)
  
  min(df$perp)
  df$ratio_perp <- df$perp / max(df$perp)
  df$ratio_lk <- df$loglk / min(df$loglk)
  
  df <- data.frame(cbind(
    aggregate(df$ratio_perp, by=list(df$k), FUN=mean),
    aggregate(df$ratio_perp, by=list(df$k), FUN=sd)$x,
    aggregate(df$ratio_lk, by=list(df$k), FUN=mean)$x,
    aggregate(df$ratio_lk, by=list(df$k), FUN=sd)$x),
    stringsAsFactors=F)
  names(df) <- c("k", "ratio_perp", "sd_perp", "ratio_lk", "sd_lk")
  library(reshape)
  pd <- melt(df[,c("k","ratio_perp", "ratio_lk")], id.vars="k")
  pd2 <- melt(df[,c("k","sd_perp", "sd_lk")], id.vars="k")
  pd$sd <- pd2$value
  levels(pd$variable) <- c("Perplexity", "LogLikelihood")
  
  library(ggplot2)
  library(grid)
  
  p <- ggplot(pd, aes(x=k, y=value, linetype=variable))
  pq <- p + geom_line() + geom_point(aes(shape=variable),
                                     fill="white", shape=21, size=1.40) +
    geom_errorbar(aes(ymax=value+sd, ymin=value-sd), width=4) +
    scale_y_continuous("Ratio wrt worst value") +
    scale_x_continuous("Number of topics",
                       breaks=K) +
    theme_bw()
  pq
  return(pq)
}
cvLDA <- function(Ntopics,dtm,K=10) {
  folds<-cvFolds(nrow(dtm),K,1)
  perplex <- rep(NA,K)
  llk <- rep(NA,K)
  for(i in unique(folds$which)){
    cat(i, " ")
    which.test <- folds$subsets[folds$which==i]
    which.train <- {1:nrow(dtm)}[-which.test]
    dtm.train <- dtm[which.train,]
    dtm.test <- dtm[which.test,]
    lda.fit <- LDA(dtm.train, k=Ntopics, method="Gibbs",
                   control=list(verbose=50L, iter=100))
    perplex[i] <- perplexity(lda.fit,dtm.test)
    llk[i] <- logLik(lda.fit)
  }
  return(list(K=Ntopics,perplexity=perplex,logLik=llk))
}
#######################################################################

datos = read.csv("datos/tecnoempleo.csv", encoding = 'utf-8')
####limpieza de datos
datos$X=NULL
datos$fecha=substr(datos$fecha, 0, 10)
datos$fecha=as.Date(datos$fecha, '%d/%m/%Y')
datos$tecnologias=as.character(datos$tecnologias)
datos$tecnologias= str_remove_all(datos$tecnologias, "[']")
datos$tecnologias= str_remove(datos$tecnologias, ']')
datos$tecnologias= str_remove(datos$tecnologias, '\\[')
datos$tecnologias =tolower(datos$tecnologias)
datos$tecnologias = as.list(strsplit(datos$tecnologias, ","))
datos$rol=as.character(datos$rol)
for (j in 1:nrow(datos)){
  guion = findstr(datos$rol[j], "-")
  if (!is.null(guion)) {
    datos$rol[j] = substring (datos$rol[j],0, guion -1)
  }
}
datos$rol=trimws(datos$rol, which = 'both')
datos$rol=as.factor(datos$rol)
datos$ubicacion = as.character(datos$ubicacion)
for (i in 1:nrow(datos)){
  coma = findstr(datos$ubicacion[i], ",")
  if (!is.null(coma)) {
    datos$ubicacion[i] = substring (datos$ubicacion[i],0, coma -1)
  }
}
datos$ubicacion= str_remove_all(datos$ubicacion, "y otras")
datos$ubicacion= str_remove_all(datos$ubicacion, "Community Of")
datos$ubicacion=trimws(datos$ubicacion, which = 'both')
datos$ubicacion= as.factor(datos$ubicacion)
summary(datos)

####reglas de asociacion
skills=datos$tecnologias
skills=as(skills, 'transactions')

library(arules)
library(arulesViz)
reglas=apriori(skills, parameter = list(supp=0.001, conf=0.5,maxlen=10))
plot(reglas, method='graph')
#plot(reglas, method = 'graph', control=list(layout=igraph::in_circle()))

####empleos por rol y por ubicacion
library(dplyr)
roles=datos %>%
  group_by(rol) %>%
  summarise(n=n())
roles %>% 
  top_n(35,wt=n)%>%
  ggplot(., aes(x=reorder(rol, n), y=n)) + geom_col() + coord_flip() + theme_bw()


ubicaciones=datos %>% 
  group_by(ubicacion) %>%
  summarise(n=n())
ubicaciones%>%
  top_n( 35, wt=n)%>%
  ggplot(., aes(x=reorder(ubicacion, n), y=n)) + geom_col() + coord_flip() + theme_bw()       


#######text mining de los titulos
library(tm)
titulos=as.list(as.character(datos$titulo))
corpus=VCorpus(VectorSource(datos$titulo))
corpus=tm_map(corpus, stripWhitespace)
corpus=tm_map(corpus, content_transformer(tolower))
corpus=tm_map(corpus, removeWords, stopwords("spanish"))
tm_map(corpus, stemDocument)
dtm=DocumentTermMatrix(corpus)
library("SnowballC")
library("wordcloud")
library("RColorBrewer")
tdm=TermDocumentMatrix(corpus)
m <- as.matrix(tdm)
v <- sort(rowSums(m),decreasing=TRUE)
d <- data.frame(word = names(v),freq=v)
head(d, 10)
set.seed(1234)
wordcloud(words = d$word, freq = d$freq, min.freq = 1,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))
#####text mining de tecnologias
library(tm)
titulos=as.list(as.character(datos$tecnologias))
corpus=VCorpus(VectorSource(datos$tecnologias))
corpus=tm_map(corpus, stripWhitespace)
corpus=tm_map(corpus, content_transformer(tolower))
corpus=tm_map(corpus, removeWords, stopwords("spanish"))
tm_map(corpus, stemDocument)
dtm=DocumentTermMatrix(corpus)
tdm=TermDocumentMatrix(corpus)
m <- as.matrix(tdm)
v <- sort(rowSums(m),decreasing=TRUE)
d <- data.frame(word = names(v),freq=v)
head(d, 10)
set.seed(1234)
wordcloud(words = d$word, freq = d$freq, min.freq = 1,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))

library(tidytext)
datos$titulo=as.character(datos$titulo)
bigramas=as.data.frame(datos$titulo)
colnames(bigramas)[1]<-"titulo"
df <- tibble::rowid_to_column(bigramas, "ID")
bigrams = df %>%
  unnest_tokens(bigram, titulo, token = "ngrams", n = 2)
bigrams_separated <- bigrams %>%
  separate(bigram, c("word1", "word2"), sep = " ")
bigrams_filtered <- bigrams_separated  
  
bigram_counts <- bigrams_filtered %>%
  dplyr::count(word1, word2, sort = TRUE)
bigrams_united <- bigrams_filtered %>%
  unite(bigram, word1, word2, sep = " ")
bigrams_united %>%
  dplyr::count(bigram, sort = TRUE)

##visualizar bigrama
review_subject <- df %>%
  unnest_tokens(word, titulo, token = "ngrams", n = 2) 

my_stopwords <- data_frame(word = c('de', 'a'))
review_subject <- review_subject %>%
  anti_join(my_stopwords)
title_word_pairs <- review_subject %>%
  pairwise_count(word, ID, sort = TRUE, upper = FALSE)
# Nos generamos el listado de bigramas
listadoBigramas<-title_word_pairs[which(title_word_pairs$n>10),]
set.seed(1234)
title_word_pairs %>%
  filter(n >= 10) %>%
  graph_from_data_frame() %>%
  ggraph(layout = "fr") +
  geom_edge_link(aes(edge_alpha = n, edge_width = n), edge_colour = "cyan4") +
  geom_node_point(size = 5) +
  geom_node_text(aes(label = name), repel = TRUE,
                 point.padding = unit(0.2, "lines")) +
  ggtitle('Bigramas')
cdfm <- dfm(corpus(as.character(datos$titulo)), remove=stopwords('es'))
dtm <- convert(cdfm, to="topicmodels")
vis<-numeroTopicsOptimo(dtm)
vis
lda <- LDA(dtm, k = 20, method = "Gibbs",
           control = list(verbose=25L, seed = 123, burnin = 100, iter = 1000))
trms <- t(terms(lda, k=10))
terminosTopic <- tidy(lda, matrix = "beta")
terminosTopic

top_terms <- terminosTopic %>%
  group_by(topic) %>%
  top_n(5, beta) %>%
  ungroup() %>%
  arrange(topic, -beta)

# Los visualizamos
top_terms %>%
  mutate(term = reorder(term, beta)) %>%
  ggplot(aes(term, beta, fill = factor(topic))) +
  geom_col(show.legend = FALSE) +
  facet_wrap(~ topic, scales = "free") +
  coord_flip()+
  labs(x = NULL, y = "Importancia palabras en topic",
       title = paste0("Topics y sus palabras descriptivas"))



