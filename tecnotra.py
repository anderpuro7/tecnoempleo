import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd
from webdriver_manager.chrome import ChromeDriverManager


def main():
    # try:
    #     driver= webdriver.Chrome('/Driver/chromedriver')
    #     driver.get('https://google.es')
    # except Exception as e:
    #     print(e)
    driver=webdriver.Chrome(ChromeDriverManager().install())
    driver.get('https://www.tecnoempleo.com/busqueda-empleo.php?cp=,29,&pr=,2,&pagina=1')
    
    ##variable que indica cuantos empleao hay
    numero_empleos=driver.find_element_by_xpath("/html/body/section[2]/div/div[2]/section/div/h5/b").text
    numero_empleos= int(numero_empleos[:5].replace(".",""))
    ###calculo del numero de paginas, 30 empleos por pagina
    paginas=0
    if((numero_empleos//30)==0):
        paginas=int(numero_empleos/30)
        print(paginas)
    else:
        paginas=int(numero_empleos/30)+1
        print(paginas)
    
    #####bucle de captura de datos
    df= pd.DataFrame()
    for i in range(1, paginas):
        driver.get('https://www.tecnoempleo.com/busqueda-empleo.php?cp=,29,&pr=,2,&pagina='+str(i))
        for j in range(1,29):
            try:
                                                                                        #--------|-------
                titulo =driver.find_element_by_xpath("/html/body/section[2]/div/div[2]/section/article["+str(j)+"]/div[1]/div[1]/h3/a/strong").text
                compania= driver.find_element_by_xpath("/html/body/section[2]/div/div[2]/section/article["+str(j)+"]/div[1]/div[1]/div/ul/li[1]/h4/a").text
                skills= driver.find_elements_by_xpath("/html/body/section[2]/div/div[2]/section/article["+str(j)+"]/div[3]/div[1]/ul/li/a")
                ubicacion=driver.find_element_by_xpath("/html/body/section[2]/div/div[2]/section/article["+str(j)+"]/div[1]/div[1]/div/ul/li[2]").text
                fecha=driver.find_element_by_xpath("/html/body/section[2]/div/div[2]/section/article["+str(j)+"]/div[1]/div[1]/div/ul/li[3]").text
                rol=driver.find_element_by_xpath("/html/body/section[2]/div/div[2]/section/article["+str(j)+"]/div[2]").text
                a=rol.find('|')
                rol=rol[1:a-1]
                #print(a)
                #print(len(skills))
                tecnologias=[]
                for h in range(1,len(skills)):
                    texto=skills[h].text
                    tecnologias.append(texto)
                print(titulo)
                print(compania)
                lista={'titulo':titulo,
                'compania':compania,
                'tecnologias':[tecnologias],
                'ubicacion':ubicacion,
                'fecha':fecha,
                'rol':rol}
                anuncio=pd.DataFrame(lista)
                
                print(anuncio)
                #print(skills[1].text)
                print("----------")
                df=df.append(anuncio)
            except Exception as e:
                print(e)
                pass
    df.to_csv("datos/tecnoempleo.csv")

#/html/body/section[2]/div/div[2]/section/article[1]/div[1]/div[1]/h3/a/strong
#/html/body/section[2]/div/div[2]/section/article[2]/div[1]/div[1]/h3/a/strong



if __name__ == "__main__":
    main();